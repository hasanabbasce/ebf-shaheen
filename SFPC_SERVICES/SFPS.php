<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Schedule
 *
 * @author HASAN
 */

class SFPS {
   
    
    private $connection;
    
    function __construct($refConn)
    {
        $this->connection=$refConn;
    }

    public function getuniqueincreamentalkey()
    {
    	$query="select APP_CREQ_SEQ.nextval from sys.dual";
        
		$arrQuery = $this->getRecords($query);

		//return $arrQuery;
	
		 $resultArray=null;
		 if(is_array($arrQuery))
		 {
		foreach($arrQuery as $aq)
		{
			 $resultArray[] = $aq;
		}
     	}
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }

    public function getEncrpt($username, $password)
    {
         $query="select
                   UTL_RAW.CAST_TO_VARCHAR2(utl_encode.base64_encode(utl_raw.cast_to_raw(ENCRYPTION.ENCRYPT('".$username."')))) username, 
                   UTL_RAW.CAST_TO_VARCHAR2(utl_encode.base64_encode(utl_raw.cast_to_raw(ENCRYPTION.ENCRYPT('".$password."')))) password from dual";
        
		$arrQuery = $this->getRecords($query);

		//return $arrQuery;
	
		 $resultArray=null;
		 if(is_array($arrQuery))
		 {
		foreach($arrQuery as $aq)
		{
			 $resultArray[] = $aq;
		}
	    }
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }

    public function addrequest($ACR_ID,$REQUEST_DATE,$CREW_ID,$EMAIL_ID,$PASSWORD,$FCM_TOKEN,$DEVICE_ID,$STATUS)
    {
    	    	$strSql= "
				INSERT INTO 
							SFPS.APP_CREW_REQUEST 
							(ACR_ID , REQUEST_DATE, CREW_ID, EMAIL_ID, PASSWORD,FCM_TOKEN, DEVICE_ID, STATUS) 
				VALUES 
							( '$ACR_ID' , TO_DATE('$REQUEST_DATE', 'DD-MM-YYYY HH24:MI:SS'), '$CREW_ID', '$EMAIL_ID', '$PASSWORD', '$FCM_TOKEN', '$DEVICE_ID', '$STATUS' )";
		
		if($this->executeQuery($strSql))
			return true;
		else
		{
			return false;
		}
    }

    public function checkstatus_crew_id($CREW_ID,$DEVICE_ID)
    {
        $query="select * from SFPS.APP_CREW_REQUEST where CREW_ID = '$CREW_ID' AND DEVICE_ID = '$DEVICE_ID' AND STATUS != 'V'";

       
        
		$arrQuery = $this->getRecords($query);

		//return $arrQuery;
	
		 $resultArray=null;
		 if(is_array($arrQuery))
		 {
			foreach($arrQuery as $aq)
			{
				 $resultArray[] = $aq;
			}
	     }
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }


    public function checkstatus_withoutdevice($CREW_ID)
    {
        $query="select * from SFPS.APP_CREW_REQUEST where CREW_ID = '$CREW_ID' AND STATUS != 'V'";

       
        
        $arrQuery = $this->getRecords($query);

        //return $arrQuery;
    
         $resultArray=null;
         if(is_array($arrQuery))
         {
            foreach($arrQuery as $aq)
            {
                 $resultArray[] = $aq;
            }
         }
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }

    

    public function checkstatus($ACR_ID)
    {
         $query="select * from SFPS.APP_CREW_REQUEST where ACR_ID = '$ACR_ID'";
        
		$arrQuery = $this->getRecords($query);

		//return $arrQuery;
	
		 $resultArray=null;
		 if(is_array($arrQuery))
		 {
			foreach($arrQuery as $aq)
			{
				 $resultArray[] = $aq;
			}
	     }
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }




    public function fetchuser($CREW_ID)
    {
         $query="select CP.CREW_ID,
                        CP.FIRST_NAME,
                        CP.MIDDLE_NAME,
                        CP.LAST_NAME,
                        CP.DOB,
                        CP.FATHER_NAME,
                        CP.MARITAL_STATUS,
                        CT.CREW_TYPE,
                        CP.GENDER,
                        CP.CCCC_NO,
                        CONCAT(CG.GROUP_CODE , CONCAT('-',CG.DESCRIPTION)) AS CREW_GROUP ,
                        CB.BATCH_NO ,
                        CP.CAAF_NO,
                        CP.PP_NAME,
                        CP.LOGIN_NAME,
                        CP.PASSWORD,
                        CP.CONF_PASSWORD,
                        CP.CONT_HR_YEAR,
                        CREW_POSITION.POSITION,
                        CREW_RANK.RANK,
                        CP.ADDRESS_1,
                        CP.ADDRESS_2,
                        CITY.CITY_NAME,
                        CP.PROVINCE,
                        CP.POSTAL_CODE,
                        CP.RES_PHONE_1,
                        CP.BUS_NO_1,
                        CP.MOBILE_1,
                        CP.EMAIL_1,
                        CP.RES_PHONE_2,
                        CP.BUS_NO_2,
                        CP.MOBILE_2,
                        CP.EMAIL_2,
                        CP.SWAP_CONT,
                        CP.LAST_QUA,
                        CP.OTHER_QUA,
                        CP.KIN_NAME,
                        CP.RELATION,
                        CP.KIN_ADD,
                        CP.KIN_TEL,
                        CP.CONT_DATE,
                        CP.DOJ,
                        CB.CITY_NAME CREW_BASE,
                        CP.NATIONALITY,
                        CP.PASSPORT_NO,
                        CP.ISSUE_DATE,
                        CP.EXPIRE_DATE,
                        CP.CNIC_NO,
                        CP.CNIC_ISSUE_DATE,
                        CP.CNIC_EXPIRE_DATE,
                        CP.ON_MAT_LEV,
                        CP.GIC,
                        CP.DCC,
                        CP.OTHER_HUB,
                        CP.EXP_PILOT,
                        CP.COVER_PILOT_REG,
                        CP.LVP,
                        CP.TRN,
                        CP.ETOP,
                        CP.RHS,
                        CP.BLUE_LINE,
                        CP.TRAINEE,
                        CP.TRAINER_START_DATE,
                        CP.LAST_WORK_DAY


                        from CREW_PROFILE CP
                                        left outer join CREW_TYPE CT on CP.CREW_TYPE_ID = CT.CREW_TYPE_ID
                                        left outer join CREW_CATEGORY CC on  CT.CREW_CAT_ID = CC.CREW_CAT_ID 
                                        left outer join CREW_GROUP CG on  CG.CREW_GROUP_ID = CP.CREW_GROUP_ID 
                                        left outer join CREW_BATCH CB on  CB.CREW_BATCH_ID = CP.CREW_BATCH_ID 
                                        left outer join CITY  on  CITY.CITY_ID = CP.CITY_ID
                                        left outer join CITY CB  on  CB.CITY_ID = CP.CREW_BASE
                                        left outer join CREW_POSITION  on  CREW_POSITION.CREW_POSITION_ID = CP.CREW_POSITION_ID 
                                        left outer join CREW_RANK on  CREW_RANK.RANK_ID = CP.RANK_ID
                                        where CP.CREW_ID = ".$CREW_ID ;
        
		$arrQuery = $this->getRecords($query);

		//return $arrQuery;
	
		 $resultArray=null;
		foreach($arrQuery as $aq)
		{
			 $resultArray[] = $aq;
		}
        if(is_null($resultArray))
        {
            return -1;
        }

        $finalarray = array();
        $idinfo = array();
        array_push($idinfo,array('key' => "I.D Info"));
        array_push($idinfo,array('Crew ID' => $resultArray[0]["CREW_ID"]));
        array_push($idinfo,array('First Name' => $resultArray[0]["FIRST_NAME"]));
        array_push($idinfo,array('Middle Name' => $resultArray[0]["MIDDLE_NAME"]));
        array_push($idinfo,array('Last Name' => $resultArray[0]["LAST_NAME"]));
        array_push($idinfo,array('D O B' => $resultArray[0]["DOB"]));
        array_push($idinfo,array('Father Name' => $resultArray[0]["FATHER_NAME"]));
        array_push($idinfo,array('Marital Status' => $resultArray[0]["MARITAL_STATUS"]));
        array_push($idinfo,array('Crew Type' => $resultArray[0]["CREW_TYPE"]));
        array_push($idinfo,array('Gender' => $resultArray[0]["GENDER"]));
        array_push($idinfo,array('CCCC NO' => $resultArray[0]["CCCC_NO"]));
        array_push($idinfo,array('Crew Group' => $resultArray[0]["CREW_GROUP"]));
        array_push($idinfo,array('Crew Batch' => $resultArray[0]["BATCH_NO"]));
        array_push($idinfo,array('CAAF NO' => $resultArray[0]["CAAF_NO"]));
        array_push($idinfo,array('Name On Passport' => $resultArray[0]["PP_NAME"]));
        array_push($idinfo,array('Contract Hours/Year' => $resultArray[0]["CONT_HR_YEAR"]));
        array_push($idinfo,array('Mgt Crew Position' => $resultArray[0]["POSITION"]));
        array_push($idinfo,array('Rank' => $resultArray[0]["RANK"]));
        array_push($finalarray,$idinfo);

       /* $webaccess = array();
        array_push($webaccess,array('key' => "Web Access"));
        array_push($webaccess,array('Login ID' => $resultArray[0]["LOGIN_NAME"]));
        array_push($webaccess,array('Password' => $resultArray[0]["PASSWORD"]));
        array_push($webaccess,array('Confirm Password' => $resultArray[0]["CONF_PASSWORD"]));
        array_push($finalarray,$webaccess);
        */

        $resAddr = array();
        array_push($resAddr,array('key' => "Residential Address"));
        array_push($resAddr,array('AddressLine 1' => $resultArray[0]["ADDRESS_1"]));
        array_push($resAddr,array('AddressLine 2' => $resultArray[0]["ADDRESS_2"]));
        array_push($resAddr,array('City' => $resultArray[0]["CITY_NAME"]));
        array_push($resAddr,array('Province' => $resultArray[0]["PROVINCE"]));
        array_push($resAddr,array('Postal Code' => $resultArray[0]["Postal Code"]));
        array_push($finalarray,$resAddr);

        $contact1 = array();
        array_push($contact1,array('key' => "Contact 1"));
        array_push($contact1,array('Residence Number' => $resultArray[0]["RES_PHONE_1"]));
        array_push($contact1,array('Business Number' => $resultArray[0]["BUS_NO_1"]));
        array_push($contact1,array('Mobile Number' => $resultArray[0]["MOBILE_1"]));
        array_push($contact1,array('Email' => $resultArray[0]["EMAIL_1"]));
        array_push($finalarray,$contact1);

        $contact2 = array();
        $contact2["key"] = "Contact 2";
        array_push($contact2,array('key' => "Contact 2"));
        array_push($contact2,array('Residence Number' => $resultArray[0]["RES_PHONE_2"]));
        array_push($contact2,array('Business Number' => $resultArray[0]["BUS_NO_2"]));
        array_push($contact2,array('Mobile Number' => $resultArray[0]["MOBILE_2"]));
        array_push($contact2,array('Email' => $resultArray[0]["EMAIL_2"]));
        array_push($contact2,array('Swap Contact' => $resultArray[0]["SWAP_CONT"]));
        array_push($finalarray,$contact2);

        $qualification = array();
        array_push($qualification,array('key' => "Qualification"));
        array_push($qualification,array('Last Qualification' => $resultArray[0]["LAST_QUA"]));
        array_push($qualification,array('Other Details' => $resultArray[0]["OTHER_QUA"]));
        array_push($finalarray,$qualification);

        $nextofkin = array();
        array_push($nextofkin,array('key' => "Next of KIN"));
        array_push($nextofkin,array('Name' => $resultArray[0]["KIN_NAME"]));
        array_push($nextofkin,array('Relationship' => $resultArray[0]["RELATION"]));
        array_push($nextofkin,array('Address' => $resultArray[0]["KIN_ADD"]));
        array_push($nextofkin,array('Telephone' => $resultArray[0]["KIN_TEL"]));
        array_push($finalarray,$nextofkin);

        $employment = array();
        array_push($employment,array('key' => "Employment"));
        array_push($employment,array('Date of Contract' => $resultArray[0]["CONT_DATE"]));
        array_push($employment,array('Date of Joining (Local)' => $resultArray[0]["DOJ"]));
        array_push($employment,array('Crew Base' => $resultArray[0]["CREW_BASE"]));
        array_push($employment,array('Nationality' => $resultArray[0]["NATIONALITY"]));
        array_push($employment,array('Passport Number' => $resultArray[0]["PASSPORT_NO"]));
        array_push($employment,array('Passport Issue Date' => $resultArray[0]["ISSUE_DATE"]));
        array_push($employment,array('Passport Expire Date' => $resultArray[0]["EXPIRE_DATE"]));
        array_push($employment,array('CNIC NO' => $resultArray[0]["CNIC_NO"]));
        array_push($employment,array('CNIC Issue Date' => $resultArray[0]["CNIC_ISSUE_DATE"]));
        array_push($employment,array('CNIC Expire Date' => $resultArray[0]["CNIC_EXPIRE_DATE"]));
        array_push($finalarray,$employment);


        $Other= array();
        array_push($Other,array('key' => "Other"));
        if($resultArray[0]["GENDER"] != 'M')
        {
        array_push($Other,array('On Matemity Leave' => $resultArray[0]["ON_MAT_LEV"]));
        }
        array_push($Other,array('GIC' => $resultArray[0]["GIC"]));
        array_push($Other,array('DCC' => $resultArray[0]["DCC"]));
        array_push($Other,array('Other Hubs' => $resultArray[0]["OTHER_HUB"]));
        array_push($Other,array('EXP Pilot' => $resultArray[0]["EXP_PILOT"]));
        array_push($Other,array('Cover Pilot Req' => $resultArray[0]["COVER_PILOT_REG"]));
        array_push($Other,array('Trainee' => $resultArray[0]["TRAINEE"]));
        array_push($Other,array('Last Working Day' => $resultArray[0]["LAST_WORK_DAY"]));
        array_push($Other,array('Trainer Start Date' => $resultArray[0]["TRAINER_START_DATE"]));
        array_push($Other,array('LVP' => $resultArray[0]["LVP"]));
        array_push($Other,array('TRN' => $resultArray[0]["TRN"]));
        array_push($Other,array('ETOP' => $resultArray[0]["ETOP"]));
        array_push($Other,array('RHS' => $resultArray[0]["RHS"]));
        array_push($Other,array('Blue Line' => $resultArray[0]["BLUE_LINE"]));
        array_push($finalarray,$Other);


        return $finalarray;
    }


    public function fetchfiles($CREW_ID)
    {
         $query ="SELECT APP_DOC_MANAGER_DET.ADD_ID , APP_DOC_MANAGER.TITLE , APP_DOC_MANAGER.FM_NAME , APP_DOC_MANAGER.FSIZE ,APP_DOC_CATEGORY.ADC_CODE  FROM APP_DOC_MANAGER_DET
                INNER JOIN APP_DOC_MANAGER ON APP_DOC_MANAGER.ADM_ID = APP_DOC_MANAGER_DET.ADM_ID
                INNER JOIN APP_DOC_CATEGORY ON APP_DOC_CATEGORY.ADC_ID = APP_DOC_MANAGER.ADC_ID
                WHERE APP_DOC_MANAGER.ACTIVE = 'Y' AND APP_DOC_MANAGER.PUBLISHED = 'Y' AND APP_DOC_MANAGER_DET.CREW_ID = ".$CREW_ID;

             
        
		$arrQuery = $this->getRecords($query);
        
		//return $arrQuery;
	
		 $resultArray=null;
		foreach($arrQuery as $aq)
		{
			 $resultArray[] = $aq;
		}
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }

    public function getpicturedata($CREW_ID)
    {
    	$query="SELECT CREW_PIC FROM CREW_PROFILE_PIC
				WHERE CREW_PROFILE_PIC.CREW_ID = ".$CREW_ID;
        // echo $query;
        // die;
		$arrQuery = $this->getRecords($query);

		//return $arrQuery;
	
		 $resultArray=null;
		foreach($arrQuery as $aq)
		{
			 $resultArray[] = $aq;
		}
        if(is_null($resultArray))
        {
            return -1;
        }

        return $resultArray;
    }

    

    public function completedownloadfile($ADD_ID,$DOWNLOAD_DATE)
    {
         $strSql = "UPDATE APP_DOC_MANAGER_DET
					SET DOWNLOADED = 'Y' , DOWNLOAD_DATE = TO_DATE('$DOWNLOAD_DATE', 'DD-MM-YYYY HH24:MI:SS')
					WHERE ADD_ID = ".$ADD_ID;


		if($this->executeupdateSql($strSql))
			return true;
		else
		{
			return false;
		}
    }


    public function reportissue($REPORT_ID,$CREW_ID,$DEVICE_ID,$REPORT_DATE,$REPORT,$REPORT_STATUS)
    {
                $strSql= "
                INSERT INTO 
                            SFPS.MOBILE_REPORT_ISSUE 
                            (REPORT_ID,CREW_ID,DEVICE_ID, REPORT_DATE, REPORT ,REPORT_STATUS) 
                VALUES 
                            ( '$REPORT_ID' , '$CREW_ID', '$DEVICE_ID' , TO_DATE('$REPORT_DATE', 'DD-MM-YYYY HH24:MI:SS'), '$REPORT', '$REPORT_STATUS')";
        
        if($this->executeQuery($strSql))
            return true;
        else
        {
            return false;
        }
    }


    public function fetchissuereports($CREW_ID,$DEVICE_ID)
    {
         $query="SELECT * FROM MOBILE_REPORT_ISSUE 
         WHERE CREW_ID = '$CREW_ID' OR DEVICE_ID = '$DEVICE_ID' ORDER BY REPORT_ID DESC";
        
        $arrQuery = $this->getRecords($query);

        //return $arrQuery;
    
         $resultArray=null;
        foreach($arrQuery as $aq)
        {
             $resultArray[] = $aq;
        }
        if(is_null($resultArray))
        {
            return -1;
        }
        return $resultArray;
    }

    
    
	public function getRecords($strSql)
	{
		return fetchSqlData($strSql, $this->connection);
	}

	private function executeQuery($strSql){

		return executeSql($strSql, $this->connection);
	}
    private function executeupdateSql($strSql){

        return executeupdateSql($strSql, $this->connection);
    }

    

    
    
    
}
