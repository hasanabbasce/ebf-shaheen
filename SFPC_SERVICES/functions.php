<?php

function connect_db($strHost, $strUser, $strPswd, $intPort, $strSid){
    return ociLogon($strUser, $strPswd, "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST = $strHost)(PORT=$intPort))(CONNECT_DATA=(SERVER=DEDICATED)(SID=$strSid)))");
}

/**
 * This function will help to fetch the data from database via the sql query
 * In case of syntax error in SQL, that function will print the SQL and return false
 *
 * @param $strSqlToExecute      string                  This would be the query by which data required to fetch
 * @param $refConnection        DB Connection   On which database connection query need to be execute
 * @return $arrRows             array                   The associate array of all rows fetched (can be multiple rows)
 */
function fetchSqlData($strSqlToExecute, $refConnection, $intFetchType = OCI_ASSOC)
{//debug_print_backtrace();
	$arrRows = array();
	$refParsed = ociparse($refConnection,$strSqlToExecute);
	if(ociexecute($refParsed))
	{
	
		if(!$refParsed)
		{

			echo '<!--';
			
			print("Error in SQL: ".$strSqlToExecute);
			echo '-->';
			return false;
		}
		while(OCIFetchInto($refParsed,$arrOneRow,$intFetchType))
			$arrRows[] = $arrOneRow;
	
	}else{
		//debug_print_backtrace();
		//debug($strSqlToExecute, true);
            print_r($strSqlToExecute);
		echo '<!--';
		print("Error in SQL: ".$strSqlToExecute);
		echo '-->';
		return false;
	}

	
	return $arrRows;
}


function executeSql($strSqlToExecute, $refConnection)
{
	$strSqlToExecute = trim($strSqlToExecute);
	$arrRows = array();
	$refParsed = OCIParse($refConnection,$strSqlToExecute);
	
	//debug($strSqlToExecute);
	
	
	global $___bolOciACF;
	
	$bolAutoCommit = true;
	if(isset($___bolOciACF) && $___bolOciACF == '_')
		$bolAutoCommit = false;


	
	if(!$bolAutoCommit){


		if(!$refParsed || !ociexecute($refParsed, (int)OCI_NO_AUTO_COMMIT))
		{
			echo '<!--';
			print("Error in SQL: ".$strSqlToExecute);
			echo '-->';
			return false;
		}
	}else{
		
		if(!$refParsed || !ociexecute($refParsed)) 
		{
			echo '<!--';
			print("Error in SQL: ".$strSqlToExecute);
			echo '-->';
			return false;
		}
	}
	return true;
}

function executeupdateSql($strSqlToExecute, $refConnection)
{
	$strSqlToExecute = trim($strSqlToExecute);

	$arrRows = array();
	$refParsed = OCIParse($refConnection,$strSqlToExecute);


	$result = ociexecute($refParsed,(int)OCI_COMMIT_ON_SUCCESS);
	if (!$result) {
	  echo oci_error();   
	  return false;
	}
	
	return true;
}

/**
 * For Insert and Update on data
 */
function dataOperation($arrData, $strTable, $refDbConn, $chrOpCode = 'I', $strSequenceNameOrWhrCluase = '')
{

    if($chrOpCode == 'I')
    {
        
        $strPKField = getPrimaryKeyField($strTable, $refDbConn);
       
        $strInsertSql = "INSERT INTO $strTable ";
        
		$intNextVal = true; //default value
        
        if(!empty($strSequenceNameOrWhrCluase) && !empty($strPKField)){
            
            $strNextSeqSql = "SELECT $strSequenceNameOrWhrCluase.NEXTVAL N_V FROM DUAL";
            $arrNextValData = fetchSqlData($strNextSeqSql, $refDbConn);
            $intNextVal = $arrNextValData[0]['N_V'];
            
            
            $strColumnsPart = "( $strPKField, ";
            $strValuesPart  = "VALUES ( $intNextVal, ";
        }else{
            $strColumnsPart = "( ";
            $strValuesPart  = "VALUES ( ";
        }
        
        foreach($arrData as $key => $val)
        {
            $strColumnsPart .= "$key, ";
			
            if(is_array($val)){
                if($val['T'] == "E")
                    $strValuesPart  .= $val['E'].", ";
                else
                    $strValuesPart  .= "'$val', ";


            }else{
                if($val == "SYSDATE")
                    $strValuesPart  .= "$val, ";
                else
                    $strValuesPart  .= "'$val', ";
            
            }
            
            
        }
        
        $strColumnsPart = substr($strColumnsPart, 0, -2);
        $strValuesPart = substr($strValuesPart, 0, -2);
        
        $strInsertSql = $strInsertSql.$strColumnsPart." ) ".$strValuesPart." )";
  //       debug($strInsertSql);

        //SELECT FOO_ID_SEQ.CURRVAL FROM DUAL
        /*if(!empty($strSequenceNameOrWhrCluase))
            $strInsertSql .= "; SELECT $strSequenceNameOrWhrCluase.CURRVAL FROM DUAL ";*/
        
//        debug($strInsertSql);
    
        if(executeSql($strInsertSql, $refDbConn))
            return $intNextVal;
        return false;
    
    }elseif($chrOpCode == 'U' && !empty($strSequenceNameOrWhrCluase)){
        
        $strUpdateSql = "UPDATE $strTable SET ";
        foreach($arrData as $key => $val){
                if(is_array($val)){
                
                $strColumnsPart .= "$key = ";
                if($val['T'] == "E")
                    $strColumnsPart  .= $val['E'].", ";
                else
                    $strColumnsPart  .= "'$val', ";

			}else{
		
			$strColumnsPart .= "$key = '$val', ";
			}
		}
		$strColumnsPart = substr($strColumnsPart, 0, -2);
		
		$strUpdateSql .= $strColumnsPart." WHERE ".$strSequenceNameOrWhrCluase;
		
		
		//debug($strUpdateSql);
		return executeSql($strUpdateSql, $refDbConn);
	}
}


function getPrimaryKeyField($strTableName, $refConn)
{
	//separate "
	$strTableName = strtoupper(substr($strTableName, strpos($strTableName,".")+1));
	
	$strPrimaryKeySql = "
					SELECT 
						cols.column_name, 
						cons.status
					FROM 
						all_constraints cons, 
						all_cons_columns cols
					WHERE 
						cons.constraint_type = 'P'
						AND cons.constraint_name = cols.constraint_name
						AND  cols.table_name = '$strTableName'
	";
	//debug($strPrimaryKeySql);
	$arrColumnsInfo = fetchSqlData($strPrimaryKeySql, $refConn);
	
	//debug($arrColumnsInfo);


	return $arrColumnsInfo[0]['COLUMN_NAME'];
}