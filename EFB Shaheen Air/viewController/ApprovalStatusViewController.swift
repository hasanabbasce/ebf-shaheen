//
//  ApprovalStatusViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 21/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ApprovalStatusViewController: UIViewController {
    
    @IBOutlet weak var approvalstatusmessage: UILabel!
    
    
    @IBOutlet weak var requestagainbtn: UIButton!
    
    
    var rejectmessge:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(rejectmessge != nil)
        {
            approvalstatusmessage.text = rejectmessge
        }
        if let REJECTED = UserDefaults.standard.value(forKey: "REJECTED") as! String? {
           // requestagainbtn.isHidden = true
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func requestagain(_ sender: Any) {
        if let ACR_ID = UserDefaults.standard.value(forKey: "ACR_ID") as! String? {
            
            let askAlert = UIAlertController(title: "New Request", message: "Are you sure you want to generate new request your previous request will be discard?", preferredStyle: UIAlertControllerStyle.alert)
            
            askAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                
                UserDefaults.standard.removeObject(forKey: "CREW_ID")
                UserDefaults.standard.removeObject(forKey: "REJECTED")
                UserDefaults.standard.removeObject(forKey: "ACR_ID")
                UserDefaults.standard.removeObject(forKey: "REVOKED")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.checkStatusStatus()
                
            }))
            
            askAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(askAlert, animated: true, completion: nil)
            
        }
        else
        {
            UserDefaults.standard.removeObject(forKey: "CREW_ID")
            UserDefaults.standard.removeObject(forKey: "REJECTED")
            UserDefaults.standard.removeObject(forKey: "ACR_ID")
            UserDefaults.standard.removeObject(forKey: "REVOKED")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.checkStatusStatus()
        }
        
    }
    
    
    @IBAction func reportaproblem(_ sender: Any) {
    
    
    }
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
