//
//  ProfileViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 20/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var profileimage: UIImageView!
    
    var tableData = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileimage.tintColor = UIColor.white
        tableview.dataSource = self
        tableview.delegate = self
        tableview.layoutMargins = .zero
        tableview.separatorInset = .zero
        
        let notificationName = Notification.Name("userdataset")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleuserdataset), name: notificationName, object: nil)
        renderprofile()
        
        
        let notificationName2 = Notification.Name("userpicset")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleuserpicset), name: notificationName2, object: nil)
        renderpic()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleuserdataset(withNotification notification : NSNotification) {
        renderprofile()
        
    }
    
    @objc func handleuserpicset(withNotification notification : NSNotification) {
        renderpic()
    }
    
    
    func renderpic()
    {
        if let USERPIC = UserDefaults.standard.value(forKey: "USERPIC") as? Data {
            do {
                
                self.profileimage.image = UIImage(data: USERPIC)
            }
            catch let error as NSError {
                
                print("Error in fetching Data from cache")
            }
        }
    }
    
    
    func renderprofile()
    {
        tableData = []
        if let USERDATA = UserDefaults.standard.value(forKey: "USERDATA") as? Data {
            do {
                
                let json =  try JSONSerialization.jsonObject(with: USERDATA, options: []) as? [String:Any]
                
                if let jsonobject = json!["DATA"]! as? [Any]  {
                   self.tableData = []
                   
                    for dic in jsonobject {
                       
                        if let dicobject = dic as? [Any]  {
                             self.tableData.append(dicobject)
                        }
                    }
                    self.tableview.reloadData()
                }
            }
            catch let error as NSError {
                
                print("Error in fetching Data from cache")
            }
        }
        
        
        self.tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return (tableData[section] as! [Any]).count - 1
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        myLabel.center = CGPoint(x: self.view.frame.width/2, y: 20)
        myLabel.textAlignment = .center
        print(tableData[section])
        let sectionarryobj = tableData[section] as! [Any]
        let headingobj = sectionarryobj[0] as? [String:Any]
        if let heading = headingobj!["key"] as? String
        {
            myLabel.text = heading
        }
        myLabel.textColor = hexStringToUIColor(hex: "#D6B05E")
        vw.addSubview(myLabel)
        vw.backgroundColor = hexStringToUIColor(hex: "#251704")
        return vw
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?  = tableView.dequeueReusableCell(withIdentifier: "CELL")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CELL")
        }
 
        let sectionarryobj = tableData[indexPath.section] as! [Any]
        let headingobj = sectionarryobj[indexPath.row + 1] as! [String:Any]
        if let keytoshow = Array(headingobj.keys)[0] as? String
        {
            var labletext = keytoshow + " : "
            if let texttoshow = Array(headingobj.values)[0] as? String
            {
                labletext =  labletext + texttoshow
                
            }
            cell?.textLabel?.text = labletext
        }
       
        cell?.backgroundColor = UIColor.clear
        cell?.textLabel?.textColor = UIColor.white
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

