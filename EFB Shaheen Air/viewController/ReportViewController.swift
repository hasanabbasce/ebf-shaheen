//
//  ReportViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 06/03/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ReportViewController: UIViewController ,NVActivityIndicatorViewable ,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var reporttext: UITextView!
    let activityData = ActivityData()
    var netService = Networkingservice()
    
    @IBOutlet weak var tableview: UITableView!
    
  
    var tableData = [[String:String]]()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
       tableview.register(UINib(nibName: "ReportissueTableViewCell", bundle: nil), forCellReuseIdentifier: "ReportissueTableViewCell")
        tableview.dataSource = self
        tableview.delegate = self
        tableview.layoutMargins = .zero
        tableview.separatorInset = .zero
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        renderreports()
        appDelegate.fetchissuereports(completion: { (response) in
            self.renderreports()
        })
        
        // Do any additional setup after loading the view.
    }
    
    func renderreports()
    {
        tableData = []
        if let ISSUEREPORTS = UserDefaults.standard.value(forKey: "ISSUEREPORTS") as? Data {
            do {
                
                let json =  try JSONSerialization.jsonObject(with: ISSUEREPORTS, options: []) as? [String:Any]
                
                if let jsonobject = json!["DATA"]! as? [Any]  {
                    print(jsonobject)
                    self.tableData = []
                    
                    for dic in jsonobject {
                        
                        if let dicobject = dic as? [String:String]  {
                            self.tableData.append(dicobject)
                        }
                    }
                    self.tableview.reloadData()
                    
                }
            }
            catch let error as NSError {
                
                print("Error in fetching Data from cache")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitreport(_ sender: Any) {
        
        if(self.reporttext.text == ""  )
        {
            let alert = UIAlertController(title: "Report Required", message: "Please Fill out required field.", preferredStyle: UIAlertControllerStyle.alert)
            
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("ok")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            
            return
        }
        var CREW_ID :String = ""
      if let CREW_ID_SAVED = UserDefaults.standard.value(forKey: "CREW_ID") as? String {
        CREW_ID = CREW_ID_SAVED
        }
            
            let date : Date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
            let todaysDate = dateFormatter.string(from: date)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        self.netService.reportissue(CREW_ID: CREW_ID, REPORT_DATE: todaysDate, REPORT: reporttext.text) { (response) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if let dataa = response["data"] as? String  {
            
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.fetchissuereports(completion: { (response) in
                    self.renderreports()
                })
            
            let alert = UIAlertController(title: "Report Submitted", message: "Your report have been submitted.", preferredStyle: UIAlertControllerStyle.alert)
            
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("ok")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
                
            }
            else
            {
                let alert = UIAlertController(title: "Error", message: "Some Thing went wrong.", preferredStyle: UIAlertControllerStyle.alert)
                
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("ok")
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}))
            }
            
        }
            
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ReportissueTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "ReportissueTableViewCell") as? ReportissueTableViewCell
        if cell == nil {
            
            tableView.register(UINib(nibName: "ReportissueTableViewCell", bundle: nil), forCellReuseIdentifier: "ReportissueTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "ReportissueTableViewCell") as? ReportissueTableViewCell
            
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let ReportissueCell = cell as? ReportissueTableViewCell  {
        ReportissueCell.backgroundColor = UIColor.clear
            ReportissueCell.yourquery.text = self.tableData[indexPath.row]["REPORT"]!
            ReportissueCell.admincomment.text = (self.tableData[indexPath.row]["REPORT_ADMIN_REMARKS"] ?? "")
            if(ReportissueCell.admincomment.text == "")
            {
                ReportissueCell.admincomment.isOpaque = false
                ReportissueCell.admincomment.isHidden = true
                ReportissueCell.admincommentheading.isOpaque = false
                ReportissueCell.admincommentheading.isHidden = true
            }
            else
            {
                ReportissueCell.admincomment.isOpaque = true
                ReportissueCell.admincomment.isHidden = false
                ReportissueCell.admincommentheading.isOpaque = true
                ReportissueCell.admincommentheading.isHidden = false
            }
//        cell.textLabel?.textColor = UIColor.white
//        cell.detailTextLabel?.textColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let font = UIFont(name: "Helvetica", size: 16.0)
        let heightyourquery = heightForView(text: self.tableData[indexPath.row]["REPORT"]!, font: font!, width: self.view.frame.width)
        let heightadminremarks = heightForView(text: self.tableData[indexPath.row]["REPORT_ADMIN_REMARKS"] ?? "", font: font!, width: self.view.frame.width)
        
        return heightyourquery + heightadminremarks + 80
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        myLabel.center = CGPoint(x: self.view.frame.width/2, y: 20)
        myLabel.textAlignment = .center
        myLabel.text = "Report History"
        myLabel.textColor = hexStringToUIColor(hex: "#D6B05E")
        vw.addSubview(myLabel)
        vw.backgroundColor = hexStringToUIColor(hex: "#251704")
        return vw
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
