//
//  MainTabViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 21/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.navigationController?.setNavigationBarHidden(true, animated: false)
        for i in 0...2 {
            tabBar.items?[i].tag = i
        }
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        //Code to run when a certain tab is selected
        //setupNavBar()
    }
    
    
    func setupNavBar()
    {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = hexStringToUIColor(hex: "#251704")
        self.navigationController?.view.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont(name: "Gill Sans", size: 20)!]
        
        
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "logo2"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 141, height: 60)
        button.imageView?.contentMode = UIViewContentMode.scaleToFill
         button.contentEdgeInsets = UIEdgeInsetsMake(0, -60, 0, 10)
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
       
        self.navigationItem.leftBarButtonItem = barButton
        
        
        
        if(tabBar.selectedItem?.tag == 0)
        {
//            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.addLeague(sender:)))
//            let iconsnotification = UIImage(named: "icons-add")
//            navigationItem.rightBarButtonItem?.image = iconsnotification
//
//            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.openSearchLeague(sender:)))
//            let iconssearch = UIImage(named: "icons-search")
//            navigationItem.leftBarButtonItem?.image = iconssearch
//            navigationItem.rightBarButtonItem = nil
//            navigationItem.leftBarButtonItem = nil
//            navigationItem.title = "Profile"
        }
        else if(tabBar.selectedItem?.tag == 1)
        {
//            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.addTeam(sender:)))
//            let iconsnotification = UIImage(named: "icons-add")
//            navigationItem.rightBarButtonItem?.image = iconsnotification
//
//            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Search", style: .plain, target: self, action: #selector(self.openSearchTeam(sender:)))
//            let iconssearch = UIImage(named: "icons-search")
//            navigationItem.leftBarButtonItem?.image = iconssearch
//            navigationItem.rightBarButtonItem = nil
//            navigationItem.leftBarButtonItem = nil
//            navigationItem.title = "Documents"
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
