//
//  ProfileViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 20/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import QuickLook

class DocumentListViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate , communicateDelegate , QLPreviewControllerDataSource , QLPreviewControllerDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var segmentcontroll: UISegmentedControl!
    
    var notification = [DownloadService]()
    
    var forms = [DownloadService]()
    
    var manuals = [DownloadService]()
    
    var fileurltoopen:URL?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableview.dataSource = self
        tableview.delegate = self
        tableview.layoutMargins = .zero
        tableview.separatorInset = .zero
        
        segmentcontroll.addTarget(self, action: #selector(segmentSelected(sender:)), for: .valueChanged)
        segmentcontroll.removeBorders()
        tableview.register(UINib(nibName: "DocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "DocumentTableViewCell")
        
        let notificationName = Notification.Name("filesset")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlefilesset), name: notificationName, object: nil)
        // Do any additional setup after loading the view.
        renderfiles()
        
        
        let dowloadprogress = Notification.Name("dowloadprogress")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handledowloadprogress), name: dowloadprogress, object: nil)
        
        let dowloadcomplete = Notification.Name("dowloadcomplete")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadcomplete), name: dowloadcomplete, object: nil)
        
        let dowloadpause = Notification.Name("dowloadpause")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadpause), name: dowloadpause, object: nil)
        
        let dowloadstop = Notification.Name("dowloadstop")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadstop), name: dowloadstop, object: nil)
        
        let dowloadtriggered = Notification.Name("dowloadtriggered")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadtriggered), name: dowloadtriggered, object: nil)
        
    }
    
    @objc func handlefilesset(withNotification notification : NSNotification) {
        renderfiles()
    }
    
    @objc func handleddowloadtriggered(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadService
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    
    @objc func handledowloadprogress(withNotification notification : NSNotification) {
       let dowloadobject = notification.object as? DownloadService
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    
    
    
    
    @objc func handleddowloadcomplete(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadService
        updatedocumentcell(dowloadobject:dowloadobject!)
       
    }
    @objc func handleddowloadpause(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadService
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    @objc func handleddowloadstop(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadService
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    
    
    func updatedocumentcell(dowloadobject:DownloadService)
    {
        if(dowloadobject != nil)
        {
            if let fileid = dowloadobject.fileid
            {
                if let category = dowloadobject.category
                {
                    if(category == "NOTE")
                    {
                        for index in 0..<self.notification.count
                        {
                            if(self.notification[index].fileid == fileid)
                            {
                                self.notification[index] = dowloadobject
                                if(segmentcontroll.selectedSegmentIndex == 0)
                                {
                                    let indexPath = IndexPath(item: index, section: 0)
                                    DispatchQueue.main.async {
                                        // self.tableview.reloadRows(at: [indexPath], with: .none)
                                        if let cell = self.tableview.cellForRow(at: indexPath) as? DocumentTableViewCell {
                                            cell.configureCell(Document: self.notification[index])
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                    else if(category == "MAN")
                    {
                        for index in 0..<self.manuals.count
                        {
                            if(self.manuals[index].fileid == fileid)
                            {
                                print(index)
                                self.manuals[index] = dowloadobject
                                if(segmentcontroll.selectedSegmentIndex == 2)
                                {
                                    let indexPath = IndexPath(item: index, section: 0)
                                    DispatchQueue.main.async {
                                        //self.tableview.reloadRows(at: [indexPath], with: .none)
                                        
                                        if let cell = self.tableview.cellForRow(at: indexPath) as? DocumentTableViewCell {
                                            cell.configureCell(Document: self.manuals[index])
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    else if(category == "FRM")
                    {
                        for index in 0..<self.forms.count
                        {
                            if(self.forms[index].fileid == fileid)
                            {
                                self.forms[index] = dowloadobject
                                if(segmentcontroll.selectedSegmentIndex == 1)
                                {
                                    let indexPath = IndexPath(item: index, section: 0)
                                    DispatchQueue.main.async {
                                        //self.tableview.reloadRows(at: [indexPath], with: .none)
                                        if let cell = self.tableview.cellForRow(at: indexPath) as? DocumentTableViewCell {
                                            cell.configureCell(Document: self.forms[index])
                                        }
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func segmentSelected(sender: UISegmentedControl)
    {
        tableview.reloadData()
        // Do what you want
    }
    
    func renderfiles()
    {
        notification = []
        forms = []
        manuals = []
        segmentcontroll.setTitle("Notifications", forSegmentAt: 0)
        segmentcontroll.setTitle("Forms", forSegmentAt: 1)
        segmentcontroll.setTitle("Manuals", forSegmentAt: 2)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let files = appDelegate.fetchdocs()
        let currentservices = appDelegate.currentservices
        for object in files {
         if(object.category == "NOTE")
         {
            if(currentservices[object.fileid!] != nil)
            {
                notification.append(currentservices[object.fileid!]!)
            }
            else
            {
                print(object)
            let downloadserviceobj = DownloadService.init(fileid: object.fileid!, title: object.title!, filename: object.filename!, category: object.category!)
            downloadserviceobj.filepath = object.filepath!
            downloadserviceobj.dowloadeddate = object.dowloadeddate
            downloadserviceobj.filesize = object.filesize
            downloadserviceobj.newversion = object.newversion
            downloadserviceobj.uploaddate = object.uploaddate!
            downloadserviceobj.refdoc = object.refdoc!
            downloadserviceobj.version = object.version!
            notification.append(downloadserviceobj)
            }
         }
         else if(object.category == "MAN")
         {
            if(currentservices[object.fileid!] != nil)
            {
                manuals.append(currentservices[object.fileid!]!)
            }
            else
            {
             let downloadserviceobj = DownloadService.init(fileid: object.fileid!, title: object.title!, filename: object.filename!, category: object.category!)
            downloadserviceobj.filepath = object.filepath!
                downloadserviceobj.dowloadeddate = object.dowloadeddate
                downloadserviceobj.filesize = object.filesize
                downloadserviceobj.newversion = object.newversion
                downloadserviceobj.uploaddate = object.uploaddate!
                downloadserviceobj.refdoc = object.refdoc!
                downloadserviceobj.version = object.version!
            manuals.append(downloadserviceobj)
            }
         }
         else if(object.category == "FRM")
         {
            if(currentservices[object.fileid!] != nil)
            {
                forms.append(currentservices[object.fileid!]!)
            }
            else
            {
            let downloadserviceobj = DownloadService.init(fileid: object.fileid!, title: object.title!, filename: object.filename!, category: object.category!)
            downloadserviceobj.filepath = object.filepath!
             downloadserviceobj.dowloadeddate = object.dowloadeddate
                downloadserviceobj.filesize = object.filesize
                downloadserviceobj.newversion = object.newversion
                downloadserviceobj.uploaddate = object.uploaddate!
                downloadserviceobj.refdoc = object.refdoc!
                downloadserviceobj.version = object.version!
            forms.append(downloadserviceobj)
            }
         }
            
        }
        if(notification.count > 0)
        {
            segmentcontroll.setTitle("Notifications (" + String(notification.count) + ")" , forSegmentAt: 0)
        }
        if(forms.count > 0)
        {
            segmentcontroll.setTitle("Forms (" + String(forms.count) + ")" , forSegmentAt: 1)
        }
        if(manuals.count > 0)
        {
            segmentcontroll.setTitle("Manuals (" + String(manuals.count) + ")" , forSegmentAt: 2)
        }
        print("files")
        print(files)
        print("notification")
        print(notification)
        tableview.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(segmentcontroll.selectedSegmentIndex == 0)
        {
            return notification.count
        }
        else if(segmentcontroll.selectedSegmentIndex == 1)
        {
            return forms.count
        }
        else
        {
            return manuals.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

//        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell", for: indexPath) as! DocumentTableViewCell
//        // cell.delegate = self
       
        var cell: DocumentTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell") as? DocumentTableViewCell
        if cell == nil {
            
            tableView.register(UINib(nibName: "DocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "DocumentTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell") as? DocumentTableViewCell
            
            
        }
        cell?.delegate = self
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if let DocumentCell = cell as? DocumentTableViewCell  {
            
            if(segmentcontroll.selectedSegmentIndex == 0)
            {
                DocumentCell.configureCell(Document: self.notification[indexPath.row])
            }
            else if(segmentcontroll.selectedSegmentIndex == 1)
            {
                DocumentCell.configureCell(Document: self.forms[indexPath.row])
            }
            else
            {
               
               DocumentCell.configureCell(Document: self.manuals[indexPath.row])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
      return 80
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tapbtn(sender: Any?, Cell: UITableViewCell?, identifier: String?, Data: Any?) {
        if(identifier == "viewdocument" )
        {
            let filedownload = Data as? DownloadService
            if(filedownload != nil)
            {
                if(filedownload?.filepath != nil && filedownload?.filepath != "")
                {
//                    if let viewdocumentviewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewDocumentViewController") as? ViewDocumentViewController {
//                        if let navigator = navigationController {
//
//                            viewdocumentviewcontroller.filepath = (filedownload?.filepath)!
//                            viewdocumentviewcontroller.title = filedownload?.title
//                            viewdocumentviewcontroller.navigationItem.backBarButtonItem?.title = ""
//                            navigator.pushViewController(viewdocumentviewcontroller, animated: true)
//                        }
//                    }
                    self.fileurltoopen = URL(fileURLWithPath: (filedownload?.filepath)!)
                    
                    let viewPDF = QLPreviewController()
                    viewPDF.dataSource = self
                    if let navigator = navigationController {
                        viewPDF.navigationItem.backBarButtonItem?.title = ""
                        navigator.pushViewController(viewPDF, animated: true)
                    }
                    
                  
                    
                  
                }
            }
        }
        if(identifier == "resetdocument" )
        {
            
            
                    let askAlert = UIAlertController(title: "Restart Download", message: "Are you sure you want to restart this download ?", preferredStyle: UIAlertControllerStyle.alert)
            
                    askAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                        
                        let documentobj = Data as? DownloadService
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        if(appDelegate.networkavalaible)
                        {
                            appDelegate.startnewservice(fileid: (documentobj?.fileid)!)
                        }
                        
                    }))
            
                    askAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                        print("Handle Cancel Logic here")
                    }))
            
                    present(askAlert, animated: true, completion: nil)
        }
        
        if(identifier == "stopdocument" )
        {
        
            
            let askAlert = UIAlertController(title: "Stop Download", message: "Are you sure you want to stop this download ?", preferredStyle: UIAlertControllerStyle.alert)
            
            askAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                
                let documentobj = Data as? DownloadService
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.stopservice(fileid: (documentobj?.fileid)!)
                
            }))
            
            askAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(askAlert, animated: true, completion: nil)
        }
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
//        let path = Bundle.main.path(forResource: "my", ofType: "pdf")
//        let url = NSURL.fileURL(withPath: path!)
        return self.fileurltoopen as! QLPreviewItem
    }
    
    func previewControllerWillDismiss(_ controller: QLPreviewController) {
        self.dismiss(animated: true, completion: nil)
    }

    
 
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

