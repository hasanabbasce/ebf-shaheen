//
//  AppDelegate.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 16/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , MessagingDelegate , UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    var currentservices = [String:DownloadService]()
    
    var netService = Networkingservice()
    var seconds = 600
    var timer = Timer()
    var isuserapproved:Bool = false
    var firstlaunch:Bool = true
    var recursivetriggered:Bool = false
    var networkavalaible:Bool = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        UserDefaults.standard.removeObject(forKey: "ACR_ID")
//        UserDefaults.standard.removeObject(forKey: "REJECTED")
//        UserDefaults.standard.removeObject(forKey: "CREW_ID")
        // Override point for customization after application launch.
        
        
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
//        var fileSize: String = ByteCountFormatter.string(fromByteCount: Int64(10242132 ?? 0), countStyle: .file)
//        print("fileSize: \(fileSize)")
//        print("ssss")
        
        
        UITabBar.appearance().barTintColor = hexStringToUIColor(hex: "#D6B05E")
        UIApplication.shared.statusBarStyle = .lightContent
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor))
        {
            statusBar.backgroundColor = hexStringToUIColor(hex: "#D6B05E")
        }
        
        
        FirebaseApp.configure()
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        
        checkStatusStatus()
        nexttodownload()
        
        fetchissuereports { (response) in
            
        }
        
        let notificationName = Notification.Name("filesset")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlefilesset), name: notificationName, object: nil)
        return true
    }
    
    @objc func handlefilesset(withNotification notification : NSNotification) {
        if(!recursivetriggered)
        {
            nexttodownload()
        }
        
    }
    
    
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("didReceive FCM TOKEN:" + fcmToken)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Refresh FCM TOKEN:" + fcmToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
      //  checkStatusStatus()
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    
    func checkStatusStatus()
    {
        if let ACR_ID = UserDefaults.standard.value(forKey: "ACR_ID") as! String? {
            isuserapproved = false
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "ApprovalStatusViewController") as! ApprovalStatusViewController
            self.window?.rootViewController = homeVC
        }
        else if let CREW_ID = UserDefaults.standard.value(forKey: "CREW_ID") as! String? {
             timer.invalidate()
            if(self.firstlaunch)
            {
                self.firstlaunch = false
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let homeVC = storyboard.instantiateViewController(withIdentifier: "MainNavigationViewController") as! MainNavigationViewController
                self.window?.rootViewController = homeVC
            }
            self.netService.checkstatus_crew_id(CREW_ID: CREW_ID, completion: { (statusresponse) in
                
                if((statusresponse["data"] as? NSNumber) != -1)
                {
                if (statusresponse["data"] as? String) != nil
                 {
                    self.netService.getuser(CREW_ID: CREW_ID, completion: { (response) in
                        
                    })
                    
                    self.netService.getpicturedata(CREW_ID: CREW_ID, completion: { (response) in
                        
                    })
                    
                    self.netService.getfiles(CREW_ID: CREW_ID, completion: { (response) in
                        
                    })
                    
                    
                    
//                    if(self.firstlaunch)
//                    {
//                        self.firstlaunch = false
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let homeVC = storyboard.instantiateViewController(withIdentifier: "MainNavigationViewController") as! MainNavigationViewController
//                        self.window?.rootViewController = homeVC
//                    }
                }
                else if((statusresponse["data"] as? Any) != nil)
                {
                    self.unRegisterDevice()
                }
                }
                else
                {
//                    if(self.firstlaunch)
//                    {
//                        self.firstlaunch = false
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let homeVC = storyboard.instantiateViewController(withIdentifier: "MainNavigationViewController") as! MainNavigationViewController
//                        self.window?.rootViewController = homeVC
//                    }
                }
            
            
                
                })
        }
        else if let REJECTED = UserDefaults.standard.value(forKey: "REJECTED") as! String? {
            timer.invalidate()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "ApprovalStatusViewController") as! ApprovalStatusViewController
            homeVC.rejectmessge = "Your Request Have Rejected"
            self.window?.rootViewController = homeVC
             isuserapproved = false
        }
        else if let REVOKED = UserDefaults.standard.value(forKey: "REVOKED") as! String? {
            timer.invalidate()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "ApprovalStatusViewController") as! ApprovalStatusViewController
            homeVC.rejectmessge = "Your Request Have REVOKED Please Request Again"
            self.window?.rootViewController = homeVC
            isuserapproved = false
        }
        else
        {
            isuserapproved = false
            timer.invalidate()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "LoginNavigationViewController") as! LoginNavigationViewController
            self.window?.rootViewController = homeVC
        }
    
    }
    
    @objc func updateTimer() {
    
        if let ACR_ID = UserDefaults.standard.value(forKey: "ACR_ID") as! String? {
            if(isuserapproved == false)
            {
            self.netService.checkstatus(ACR_ID: ACR_ID) { (response) in
               
                if let statusdata = response["data"] as? [String:String]  {
                    
                    if(statusdata["STATUS"] == "A")
                    {
                        self.isuserapproved = true
                        UserDefaults.standard.removeObject(forKey: "ACR_ID")
                        UserDefaults.standard.removeObject(forKey: "REJECTED")
                        UserDefaults.standard.set(statusdata["CREW_ID"], forKey: "CREW_ID")
                        self.timer.invalidate()
                        
                        self.checkStatusStatus()
                    }
                    else if(statusdata["STATUS"] == "R")
                    {
                        UserDefaults.standard.removeObject(forKey: "CREW_ID")
                        UserDefaults.standard.removeObject(forKey: "ACR_ID")
                        var rejectreason = ""
                        
                        if let REJECT_REASON = statusdata["REJECT_REASON"] as? String
                        {
                            rejectreason = REJECT_REASON
                        }
                        UserDefaults.standard.set(rejectreason, forKey: "REJECTED")
                        self.timer.invalidate()
                        self.checkStatusStatus()
                    }
                    else if(statusdata["STATUS"] == "V")
                    {
                        UserDefaults.standard.removeObject(forKey: "CREW_ID")
                        UserDefaults.standard.removeObject(forKey: "ACR_ID")
                        var revokereason = ""
                        
                        
                        UserDefaults.standard.set(revokereason, forKey: "REVOKED")
                        self.timer.invalidate()
                        self.checkStatusStatus()
                    }
                    
                }
                else if(response["data"] == nil )
                {
                    UserDefaults.standard.removeObject(forKey: "ACR_ID")
                    UserDefaults.standard.removeObject(forKey: "REJECTED")
                    UserDefaults.standard.removeObject(forKey: "CREW_ID")
                    UserDefaults.standard.removeObject(forKey: "REVOKED")
                    self.checkStatusStatus()
                }
            
                }
            }
        }
        
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        checkStatusStatus()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        self.startMonitoring()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "EFB Shaheen Air")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func checkadddoc(fileid:String , title: String,filename: String,filesize: String,category:String,newversion:Bool,uploaddate:String,refdoc:String,version:String,adm_id:String)
    {
        var isexist : Bool = false
        
        let context = self.persistentContainer.viewContext
        let request = NSFetchRequest<EFBDocument>(entityName: "EFBDocument")
        do {
            let searchResults = try context.fetch(request)
            for efbdocument in searchResults {
                if efbdocument.fileid == fileid {
                    // update here
                    efbdocument.title = title
                    efbdocument.filename = filename
                    efbdocument.category = category
                    efbdocument.filesize = filesize
                    
                    isexist = true
                }
            }
        } catch {
            print("Error with request: \(error)")
        }
        self.saveContext()
        
        
        if(!isexist)
        {
            self.adddoc(fileid:fileid , title: title,filename: filename,filesize: filesize,category:category,newversion:newversion,uploaddate:uploaddate,refdoc:refdoc,version:version,adm_id:adm_id)
        }
        
    }
    
    
    func adddoc(fileid:String , title: String,filename: String,filesize: String,category:String,newversion:Bool,uploaddate:String,refdoc:String,version:String,adm_id:String)
    {
      let context = self.persistentContainer.viewContext
      let efbdocument = EFBDocument(context:context)
      efbdocument.fileid = fileid
      efbdocument.title = title
      efbdocument.filename = filename
      efbdocument.downloaded = false
      efbdocument.filepath = ""
      efbdocument.category = category
      efbdocument.dowloadeddate = ""
      efbdocument.filesize = filesize
      efbdocument.newversion = newversion
      efbdocument.uploaddate = uploaddate
      efbdocument.refdoc = refdoc
      efbdocument.version = version
      efbdocument.adm_id = adm_id
      self.saveContext()
    }
    
    func updatedoc(fileid: String,downloaded:Bool,filepath:String,dowloadeddate:String) {
        let context = self.persistentContainer.viewContext
        let request = NSFetchRequest<EFBDocument>(entityName: "EFBDocument")
        do {
            let searchResults = try context.fetch(request)
            for efbdocument in searchResults {
                if efbdocument.fileid == fileid {
                    efbdocument.filepath = filepath
                    efbdocument.downloaded = downloaded
                    efbdocument.dowloadeddate = dowloadeddate
                    efbdocument.newversion = false
                    // update here
                }
            }
        } catch {
            print("Error with request: \(error)")
        }
        self.saveContext()
    }
    
    
    func deletedoc(fileid: String) {
         let context = self.persistentContainer.viewContext
        let request = NSFetchRequest<EFBDocument>(entityName: "EFBDocument")
        do {
            let searchResults = try context.fetch(request)
            for efbdocument in searchResults {
                print(efbdocument.adm_id)
                if efbdocument.adm_id == fileid {
                    // delete task
                    if(efbdocument.filepath != nil && efbdocument.filepath != "")
                    {
                        
                        do {
                            let fileManager = FileManager.default
                            try fileManager.removeItem(atPath: efbdocument.filepath!)
                        }
                        catch
                        {
                            print("Could not clear temp folder: \(error)")
                        }
                    }
                    context.delete(efbdocument)
                }
            }
        } catch {
            print("Error with request: \(error)")
        }
        self.saveContext()
        
        let notificationName = Notification.Name("filesset")
        NotificationCenter.default.post(name: notificationName, object: nil)
        NotificationCenter.default.removeObserver(self, name: notificationName, object: nil)
    }
    
    func deletealldoc() {
        let context = self.persistentContainer.viewContext
        let request = NSFetchRequest<EFBDocument>(entityName: "EFBDocument")
        do {
            let searchResults = try context.fetch(request)
            for efbdocument in searchResults {
                if(efbdocument.filepath != nil && efbdocument.filepath != "")
                {
                
                do {
                let fileManager = FileManager.default
                    try fileManager.removeItem(atPath: efbdocument.filepath!)
                }
                catch
                {
                    print("Could not clear temp folder: \(error)")
                }
                }
                
                    context.delete(efbdocument)
            }
        } catch {
            print("Error with request: \(error)")
        }
        self.saveContext()
    }
    
    func fetchdocs() -> [EFBDocument] {
        let context = self.persistentContainer.viewContext
        var efbdocuments = [EFBDocument]()
        do {
           efbdocuments = try context.fetch(EFBDocument.fetchRequest())
        }catch {
            print("Error fetching data from CoreData")
        }
        return efbdocuments
    }
    
    
    func nexttodownload()
    {

        
        let files = self.fetchdocs()
       
        
        for object in files {
            
            print(object.filepath)
             print(object.downloaded)
            
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: object.filepath!) {
                print("already here")
                
            } else {
                print(object.newversion)
                if(currentservices[object.fileid!] == nil && object.newversion == false)
                {
                
                let downloadd = DownloadService.init(fileid: object.fileid!, title: object.title!, filename: object.filename!,category: object.category! )
                 downloadd.filesize = object.filesize!
                    downloadd.newversion = object.newversion
                    downloadd.uploaddate = object.uploaddate!
                    downloadd.refdoc = object.refdoc!
                    downloadd.version = object.version!
                    currentservices[object.fileid!] = downloadd
                 
                recursivetriggered = true
                downloadd.idlestate = true
                    let notificationtriggerName = Notification.Name("dowloadtriggered")
                    // Post notification
                    NotificationCenter.default.post(name: notificationtriggerName, object: downloadd)
                    
                    // Stop listening notification
                    NotificationCenter.default.removeObserver(self, name: notificationtriggerName, object: downloadd)
                downloadd.startDownload(completion: { (respose) in
                    downloadd.idlestate = false
                    if let Complete = respose["Complete"] as? String
                    {
                        let postDate = NSDate().timeIntervalSince1970 as NSNumber
                        downloadd.dowloadeddate = String(describing: postDate)
                        self.updatedoc(fileid: downloadd.fileid!, downloaded: true, filepath: Complete, dowloadeddate: downloadd.dowloadeddate! )
                        
                        let notificationName = Notification.Name("dowloadcomplete")
                        // Post notification
                        NotificationCenter.default.post(name: notificationName, object: downloadd)
                        
                        // Stop listening notification
                        NotificationCenter.default.removeObserver(self, name: notificationName, object: downloadd)
                        if(self.currentservices != nil && object.fileid != nil)
                        {
                        self.currentservices.removeValue(forKey: object.fileid!)
                        let date : Date = Date()
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
                        let todaysDate = dateFormatter.string(from: date)
                        self.netService.completedownloadfile(ADD_ID: downloadd.fileid, DOWNLOAD_DATE: todaysDate, completion: { (ress) in
                            
                        })
                        self.nexttodownload()
                        }
                    }
                    else if let Progress = respose["Progress"] as? Double
                    {
                        let notificationName = Notification.Name("dowloadprogress")
                        // Post notification
                        NotificationCenter.default.post(name: notificationName, object: downloadd)
                        
                        // Stop listening notification
                        NotificationCenter.default.removeObserver(self, name: notificationName, object: downloadd)
                    }
                    else
                    {
                        if(self.currentservices != nil && object.fileid != nil)
                        {
                        self.currentservices.removeValue(forKey: object.fileid!)
                        if(self.currentservices.count < 1)
                        {
                            self.recursivetriggered = false
                        }
                        downloadd.progress = 0
                        let notificationName = Notification.Name("dowloadstop")
                        // Post notification
                        NotificationCenter.default.post(name: notificationName, object: downloadd)
                        
                        // Stop listening notification
                        NotificationCenter.default.removeObserver(self, name: notificationName, object: downloadd)
                        //self.nexttodownload()
                        }
                    }
                })
                return
                }
            }
        }
        
        
    }
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        BackendAPIManager.sharedInstance.backgroundCompletionHandler = completionHandler
    }
    
    func pauseservice(fileid :String)
    {
        if(currentservices[fileid] != nil)
        {
            if(currentservices[fileid]?.request != nil)
            {
                currentservices[fileid]?.pauseDownload()
                let notificationName = Notification.Name("dowloadpause")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: currentservices[fileid])
                
                // Stop listening notification
                NotificationCenter.default.removeObserver(self, name: notificationName, object: currentservices[fileid])
            }
        }
    }
    
    func resumeservice(fileid :String)
    {
        if(currentservices[fileid] != nil)
        {
            if(currentservices[fileid]?.request != nil)
            {
                currentservices[fileid]?.resumeDownload()
            }
        }
    }
    
    func stopservice(fileid :String)
    {
        if(currentservices[fileid] != nil)
        {
            if(currentservices[fileid]?.request != nil)
            {
                currentservices[fileid]?.cancelDownload()
                let notificationName = Notification.Name("dowloadstop")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: currentservices[fileid])
                
                // Stop listening notification
                NotificationCenter.default.removeObserver(self, name: notificationName, object: currentservices[fileid])
            }
        }
    }
    
    func startnewservice(fileid:String)
    {
        
        if(currentservices[fileid] != nil)
        {
            if(currentservices[fileid]?.request != nil)
            {
                currentservices[fileid]?.cancelDownload()
                 currentservices[fileid]?.request = nil
                self.currentservices.removeValue(forKey: fileid)
            }
        }
        if(currentservices[fileid] == nil)
        {
                let files = self.fetchdocs()
                for object in files {
                    
                    if(object.fileid == fileid)
                    {
                        
                        let downloadd = DownloadService.init(fileid: object.fileid!, title: object.title!, filename: object.filename!,category: object.category! )
                        downloadd.filesize = object.filesize!
                        downloadd.newversion = object.newversion
                        downloadd.uploaddate = object.uploaddate!
                        downloadd.refdoc = object.refdoc!
                        downloadd.version = object.version!
                        currentservices[object.fileid!] = downloadd
                        
                        downloadd.idlestate = true
                        let notificationtriggerName = Notification.Name("dowloadtriggered")
                        // Post notification
                        NotificationCenter.default.post(name: notificationtriggerName, object: downloadd)
                        
                        // Stop listening notification
                        NotificationCenter.default.removeObserver(self, name: notificationtriggerName, object: downloadd)
                        downloadd.startDownload(completion: { (respose) in
                            downloadd.idlestate = false
                            if let Complete = respose["Complete"] as? String
                            {
                                let postDate = NSDate().timeIntervalSince1970 as NSNumber
                                downloadd.dowloadeddate = String(describing: postDate)
                                self.updatedoc(fileid: downloadd.fileid!, downloaded: true, filepath: Complete, dowloadeddate: downloadd.dowloadeddate! )
                                
                                //downloadd.newversion = false
                                
                                let notificationName = Notification.Name("dowloadcomplete")
                                // Post notification
                                NotificationCenter.default.post(name: notificationName, object: downloadd)
                                
                                // Stop listening notification
                                NotificationCenter.default.removeObserver(self, name: notificationName, object: downloadd)
                                self.currentservices.removeValue(forKey: object.fileid!)
                                let date : Date = Date()
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
                                let todaysDate = dateFormatter.string(from: date)
                                self.netService.completedownloadfile(ADD_ID: downloadd.fileid, DOWNLOAD_DATE: todaysDate, completion: { (ress) in
                                    
                                })
                                
                                if(downloadd.refdoc != nil && downloadd.refdoc != "")
                                {
                                    self.deletedoc(fileid: downloadd.refdoc!)
                                }
                               
                            }
                            else if let Progress = respose["Progress"] as? Double
                            {
                                let notificationName = Notification.Name("dowloadprogress")
                                // Post notification
                                NotificationCenter.default.post(name: notificationName, object: downloadd)
                                
                                // Stop listening notification
                                NotificationCenter.default.removeObserver(self, name: notificationName, object: downloadd)
                            }
                            else
                            {
                                if(object.fileid != nil)
                                {
                                self.currentservices.removeValue(forKey: object.fileid!)
                                if(self.currentservices.count < 1)
                                {
                                    self.recursivetriggered = false
                                }
                                downloadd.progress = 0
                                let notificationName = Notification.Name("dowloadstop")
                                // Post notification
                                NotificationCenter.default.post(name: notificationName, object: downloadd)
                                
                                // Stop listening notification
                                NotificationCenter.default.removeObserver(self, name: notificationName, object: downloadd)
                                }
                            }
                        })
                        return
                    }
                    
                    
                }
            
        }
        
        
        
        
    }
    
    
    func startMonitoring() {
        let reachability = Reachability()!
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        do{
            try reachability.startNotifier()
           
        } catch {
            print("Could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(notification: Notification) {
        
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            networkavalaible = false
            print("Network became unreachable")
                        for (key, obj) in currentservices {
                           
                                obj.cancelDownload()
                           self.currentservices.removeValue(forKey: key)
                        }

            
        case .wifi:
            networkavalaible = true
            print("Network reachable through WiFi")
            if(!recursivetriggered)
            {
                nexttodownload()
            }
            

            
        case .cellular:
            networkavalaible = true
            print("Network reachable through Cellular Data")
            if(!recursivetriggered)
            {
                nexttodownload()
            }
            

            
        }
    }
    
    
    func unRegisterDevice()
    {
       for (key, obj) in currentservices {
            obj.cancelDownload()
       }
       UserDefaults.standard.removeObject(forKey: "ACR_ID")
       UserDefaults.standard.removeObject(forKey: "REJECTED")
       UserDefaults.standard.removeObject(forKey: "REVOKED")
       UserDefaults.standard.removeObject(forKey: "CREW_ID")
       UserDefaults.standard.removeObject(forKey: "USERPIC")
       UserDefaults.standard.removeObject(forKey: "USERDATA")
       UserDefaults.standard.removeObject(forKey: "ISSUEREPORTS")
       deletealldoc()
       self.firstlaunch = true
       self.recursivetriggered = false
       checkStatusStatus()
        
    }
    
    
    func fetchissuereports(completion: @escaping ([String:Any?])->())
    {
        var CREW_ID : String = ""
        if let CREW_ID_saved = UserDefaults.standard.value(forKey: "CREW_ID") as! String? {
            CREW_ID = CREW_ID_saved
        }
        self.netService.getreportissues(CREW_ID: CREW_ID, completion: { (response) in
            completion(["DATA":"1"])
        })
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        checkStatusStatus()
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    
    


}

