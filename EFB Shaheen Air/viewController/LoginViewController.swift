//
//  LoginViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 16/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FirebaseMessaging

class LoginViewController: UIViewController ,NVActivityIndicatorViewable,UITextFieldDelegate {
    
    
    
    @IBOutlet weak var sai_id: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    let activityData = ActivityData()
    var netService = Networkingservice()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = hexStringToUIColor(hex: "#251704")
        self.navigationController?.view.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedStringKey.foregroundColor: UIColor.white,
             NSAttributedStringKey.font: UIFont(name: "Gill Sans", size: 20)!]
        // Do any additional setup after loading the view.
        
        sai_id.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
   
    
    @IBAction func RequestforAccess(_ sender: Any) {
        
        
        if(self.sai_id.text == "" || self.email.text == "" || self.password.text == "" )
        {
            let alert = UIAlertController(title: "All Fields Required", message: "Please Fill out all fields.", preferredStyle: UIAlertControllerStyle.alert)
            
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("ok")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            
            return
        }
        
        UserDefaults.standard.removeObject(forKey: "CREW_ID")
        UserDefaults.standard.removeObject(forKey: "REJECTED")
        UserDefaults.standard.removeObject(forKey: "ACR_ID")
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)
        
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        let todaysDate = dateFormatter.string(from: date)
        self.netService.authcheck(username: self.email.text, password: self.password.text) { (response) in
            
            
            if let data = response["data"] as? String  {
                
            if(data == "1")
            {
                self.netService.addrequest(REQUEST_DATE: todaysDate, CREW_ID: self.sai_id.text, EMAIL_ID: self.email.text, PASSWORD: self.password.text!, FCM_TOKEN: Messaging.messaging().fcmToken!, STATUS: "P") { (responsee) in
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    
                    if let dataa = responsee["data"] as? String  {
                        
                        if(dataa != "-1")
                        {
                         UserDefaults.standard.removeObject(forKey: "CREW_ID")
                         UserDefaults.standard.removeObject(forKey: "REJECTED")
                         UserDefaults.standard.set(dataa, forKey: "ACR_ID")
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.checkStatusStatus()
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Device not Allowed", message: "Please Request for revoke your previous device.", preferredStyle: UIAlertControllerStyle.alert)
                            
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("ok")
                                case .cancel:
                                    print("cancel")
                                case .destructive:
                                    print("destructive")
                                }}))
                        }
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Error", message: "Error occur while requesting for access.", preferredStyle: UIAlertControllerStyle.alert)
                        
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("ok")
                            case .cancel:
                                print("cancel")
                            case .destructive:
                                print("destructive")
                            }}))
                    }
                    
                    
                    
                }
                
            }
            else
            {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
                let alert = UIAlertController(title: "Invalid Credentials", message: "Your username and password is not correct.", preferredStyle: UIAlertControllerStyle.alert)
                
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("ok")
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}))
            }
                
                
            }
            else
            {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
                let alert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.alert)
                
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("ok")
                    case .cancel:
                        print("cancel")
                    case .destructive:
                        print("destructive")
                    }}))
            }
            
            
        }
        
        
        
        
        
    }
    
    @IBAction func termscondition(_ sender: Any) {
        
        if let termsviewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as? TermsViewController {
            if let navigator = navigationController {
                
                termsviewcontroller.title = "Terms & Conditions"
                termsviewcontroller.navigationItem.backBarButtonItem?.title = ""
                navigator.pushViewController(termsviewcontroller, animated: true)
            }
        }
    }
    
    
    @IBAction func reportaproblem(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        ReportViewController.title = "Report a Problem"
        self.navigationController?.pushViewController(ReportViewController, animated: true)
    }
    
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        if(textField == sai_id)
        {
            let currentCharacterCount = textField.text?.characters.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.characters.count - range.length
            if(newLength <= 13)
            {
                let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
            }
            else
            {
                return false
            }
        }
        return true
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
