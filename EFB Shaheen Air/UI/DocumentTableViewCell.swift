//
//  DocumentTableViewCell.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 21/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class DocumentTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var filetitle: UILabel!
    
    @IBOutlet weak var filepath: UILabel!
    
    @IBOutlet weak var progressbar: UIProgressView!
    
    @IBOutlet weak var viewbtn: UIButton!
    
    @IBOutlet weak var pausebtn: UIButton!
    
    @IBOutlet weak var playbtn: UIButton!
    
    @IBOutlet weak var downloadbtn: UIButton!
    
    @IBOutlet weak var stopbtn: UIButton!
    
    @IBOutlet weak var resetbtn: UIButton!
    
    @IBOutlet weak var estimatedtimelb: UILabel!
    
    @IBOutlet weak var downloadeddatelb: UILabel!
    
    @IBOutlet weak var sizelb: UILabel!
    
    @IBOutlet weak var extimage: UIImageView!
    
    
    @IBOutlet weak var versionlable: UILabel!
    
    
    var documentobj:DownloadService?
    
    weak var delegate:communicateDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureCell(Document:  DownloadService){
        let ext = NSURL(fileURLWithPath: Document.filename!).pathExtension
        extimage.image = UIImage(named: ext!)
        
        var documentstate : String = "nostate"
        self.documentobj = Document
        filetitle.text = Document.title
        filepath.text = Document.filename
       
        sizelb.text = "Size : " + Document.filesize!
        if(Document.progress > 0 && Document.progress < 1)
        {
            documentstate = "progressstate"
            progressbar.isHidden = false
            progressbar.progress = Float(Document.progress)
            estimatedtimelb.isHidden = false
            estimatedtimelb.text = "Progress  : " + String(format: "%.1f", (Document.progress * 100)) + " %"
           // downloadbtn.setTitle("Pause", for: .normal)
        }
        else
        {
             progressbar.progress = 0
             progressbar.isHidden = true
             estimatedtimelb.isHidden = true
        }
        
        if(Document.filepath != nil)
        {
        
        let fileManager = FileManager.default
        print(Document.filepath)
        if fileManager.fileExists(atPath: Document.filepath!) {
           print("file exist")
            documentstate = "completestate"
           // downloadbtn.setTitle("View", for: .normal)
        } else {
            
             print("not exist")
        }
            
        }
        
        downloadeddatelb.isHidden = true
        downloadeddatelb.isOpaque = false
        
        //versionlable.isHidden = true
        //versionlable.isOpaque = false
        versionlable.text = "Version : " + (self.documentobj?.version)!
        
        
        if(Document.ispause)
        {
            documentstate = "pausestate"
        }
        if(Document.downloaded)
        {
            documentstate = "completestate"
        }
        
        if(documentstate == "nostate")
        {
            nostate()
        }
        else if(documentstate == "pausestate")
        {
            pausestate()
        }
        else if(documentstate == "progressstate")
        {
            progressstate()
        }
        else if(documentstate == "completestate")
        {
            completestate()
        }
        
        if(Document.idlestate)
        {
            idlestatetrue()
        }
        else
        {
            idlestatefalse()
        }
        
      
        
    }
    
    func nostate()
    {
        stopbtn.isHidden = true
        stopbtn.isOpaque = false
        viewbtn.isHidden = true
        viewbtn.isOpaque = false
        pausebtn.isHidden = true
        pausebtn.isOpaque = false
        resetbtn.isHidden = true
        resetbtn.isOpaque = false
        playbtn.isHidden = true
        playbtn.isOpaque = false
        
        downloadbtn.isHidden = false
        downloadbtn.isOpaque = true
        
        if(documentobj?.newversion == true)
        {
        downloadbtn.tintColor = UIColor.green
        }
        else
        {
            downloadbtn.tintColor = UIColor.white
        }
    }
    
    func pausestate()
    {
        stopbtn.isHidden = true
        stopbtn.isOpaque = false
        viewbtn.isHidden = true
        viewbtn.isOpaque = false
        pausebtn.isHidden = true
        pausebtn.isOpaque = false
        downloadbtn.isHidden = true
        downloadbtn.isOpaque = false
        
        playbtn.isHidden = false
        playbtn.isOpaque = true
        resetbtn.isHidden = false
        resetbtn.isOpaque = true
        
    }
    
    func progressstate()
    {

        viewbtn.isHidden = true
        viewbtn.isOpaque = false
        downloadbtn.isHidden = true
        downloadbtn.isOpaque = false
        playbtn.isHidden = true
        playbtn.isOpaque = false
        
        pausebtn.isHidden = false
        pausebtn.isOpaque = true
        resetbtn.isHidden = false
        resetbtn.isOpaque = true
        stopbtn.isHidden = false
        stopbtn.isOpaque = true
    }
    
    func completestate()
    {
        
        downloadbtn.isHidden = true
        downloadbtn.isOpaque = false
        playbtn.isHidden = true
        playbtn.isOpaque = false
        pausebtn.isHidden = true
        pausebtn.isOpaque = false
        stopbtn.isHidden = true
        stopbtn.isOpaque = false
        
        resetbtn.isHidden = false
        resetbtn.isOpaque = true
        viewbtn.isHidden = false
        viewbtn.isOpaque = true
        
        
        if(documentobj?.dowloadeddate != nil && documentobj?.dowloadeddate != "")
        {
        
            if let dowloadeddate = (documentobj?.dowloadeddate! as? NSString)?.integerValue {
                downloadeddatelb.isHidden = false
                downloadeddatelb.isOpaque = true
                let fromDate = NSDate(timeIntervalSince1970: TimeInterval(NSNumber(value:dowloadeddate)))
                let toDate = NSDate()
                var downloadupdated : String = "Downloaded : "
                if(documentobj?.refdoc != nil && documentobj?.refdoc != "" )
                {
                    downloadupdated = "Updated : "
                }
                downloadeddatelb.text = downloadupdated + timeAgoSinceDate(fromDate as Date, currentDate: toDate as Date, numericDates: true)
            }
            
            
            
            
            
        }
        
        
    }
    
    func idlestatetrue()
    {
        
        stopbtn.isEnabled = false
        
        viewbtn.isEnabled = false
        
        pausebtn.isEnabled = false
        
        resetbtn.isEnabled = false
        
        playbtn.isEnabled = false
        
        downloadbtn.isEnabled = false
        
    }
    
    func idlestatefalse()
    {
        
        stopbtn.isEnabled = true
        
        viewbtn.isEnabled = true
        
        pausebtn.isEnabled = true
        
        resetbtn.isEnabled = true
        
        playbtn.isEnabled = true
        
        downloadbtn.isEnabled = true
        
    }
    
    
    
    @IBAction func viewact(_ sender: Any) {
        self.delegate?.tapbtn!(sender: self, Cell: self, identifier: "viewdocument", Data: self.documentobj)
    }
    
    @IBAction func pauseact(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.pauseservice(fileid: (documentobj?.fileid)!)
    }
    
    @IBAction func resumeact(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if(appDelegate.networkavalaible)
        {
        appDelegate.resumeservice(fileid: (documentobj?.fileid)!)
        }
    }
    
    @IBAction func downloadact(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print(self.documentobj)
        if(appDelegate.networkavalaible)
        {
        appDelegate.startnewservice(fileid: (documentobj?.fileid)!)
        }
    }
    
    @IBAction func stopact(_ sender: Any) {
   
        self.delegate?.tapbtn!(sender: self, Cell: self, identifier: "stopdocument", Data: self.documentobj)
    }
    
    @IBAction func resetact(_ sender: Any) {

        self.delegate?.tapbtn!(sender: self, Cell: self, identifier: "resetdocument", Data: self.documentobj)
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
