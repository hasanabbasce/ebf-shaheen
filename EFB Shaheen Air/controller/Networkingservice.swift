//
//  Networkingservice.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 20/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseMessaging


struct Networkingservice {
    
   // var apiurl = "https://turboapi.shaheenair.com/sfps/efb/"
    var apiurl = "http://172.16.24.25/sfpc_services/"
    var deviceid : String = UIDevice.current.identifierForVendor!.uuidString
    var apitoken : String = MD5(string: UIDevice.current.identifierForVendor!.uuidString + "shaheenios")
    
    
    func authcheck(username:String!,password:String!, completion: @escaping ([String:Any?])->())
    {
      
        let parameters: [String: Any] = [
            "func": "auth",
            "username": username,
            "password":password,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                
                if let json = response.result.value as? [String:Any]  {
                    
                    let data = json["DATA"] as! [String:String]
                    
                    completion(["data":data["auth"]!])
                }
            case .failure(let error):
                completion(["data":nil])
            }
            
        }
    }
    

    func addrequest(REQUEST_DATE:String!,CREW_ID:String!,EMAIL_ID:String!,PASSWORD:String,FCM_TOKEN:String,STATUS:String, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "addrequest",
            "REQUEST_DATE": REQUEST_DATE,
            "CREW_ID": CREW_ID,
            "EMAIL_ID":EMAIL_ID,
            "PASSWORD": PASSWORD,
            "FCM_TOKEN":FCM_TOKEN,
            "STATUS":STATUS,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
            
        ]
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                
                if let json = response.result.value as? [String:Any]  {
                    
                    let data = json["DATA"] as! [String:String]
                    
                    completion(["data":data["ACR_ID"]!])
                }
            case .failure(let error):
                completion(["data":nil])
            }
            
        }
    }
    
    
    func checkstatus(ACR_ID:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "checkstatus",
            "ACR_ID": ACR_ID,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                if let json = response.result.value as? [String:Any]  {
                    
                    if(json["DATA"] != nil)
                    {
                    let data = json["DATA"] as! [String:String]
                    
                    completion(["data":data])
                    }
                    else
                    {
                    completion(["data":nil])
                    }
                }
            case .failure(let error):
                completion(["data":-1])
            }
            
        }
    }
    
    func checkstatus_crew_id(CREW_ID:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "checkstatus_crew_id",
            "CREW_ID": CREW_ID,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid,
            "FCM_TOKEN":Messaging.messaging().fcmToken!
        ]
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                if let json = response.result.value as? [String:Any]  {
                    
                    if(json["DATA"] != nil)
                    {
                        let data = json["DATA"] as! String
                        
                        completion(["data":data])
                    }
                    else
                    {
                        completion(["data":nil])
                    }
                }
            case .failure(let error):
                completion(["data":-1])
            }
            
        }
    }

    
    
    func getfiles(CREW_ID:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "fetchfiles",
            "CREW_ID": CREW_ID,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                
                if let json = response.result.value as? [String:Any] {
                    
                     if let fetchfilearray = json["DATA"] as? [Any]
                     {
                          for files in fetchfilearray {
                            if let filee = files as? [String:String]
                            {
                                
                                var filesize : Int64 = 0
                                if(filee["FSIZE"] != nil)
                                {
                                    if let filesizee = Int64(filee["FSIZE"]!)
                                    {
                                        filesize = filesizee
                                    }
                                }
                                var fileSize: String = ByteCountFormatter.string(fromByteCount: Int64(filesize ?? 0), countStyle: .file)
                                    var isnewversion : Bool = false
                                var ADM_REF_ID : String = ""
                                 if(filee["ADM_REF_ID"] != nil && filee["ADM_REF_ID"] != "" )
                                 {
                                    isnewversion = true
                                    ADM_REF_ID = filee["ADM_REF_ID"]!
                                }
                              let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.checkadddoc(fileid: filee["ADD_ID"]!, title: filee["TITLE"]!, filename: filee["FM_NAME"]!, filesize: fileSize , category : filee["ADC_CODE"]!, newversion: isnewversion, uploaddate: filee["CREATION_DATE"]!, refdoc: ADM_REF_ID, version: filee["VERSION"]!,adm_id: filee["ADM_ID"]!)
                                
//                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                                    appDelegate.deletedoc(fileid: filee["ADD_ID"]!)
                                
                            }
                          }
                        
                        let notificationName = Notification.Name("filesset")
                        NotificationCenter.default.post(name: notificationName, object: nil)
                        NotificationCenter.default.removeObserver(self, name: notificationName, object: nil)
                     }
                    completion(["data":nil])
                }
            case .failure(let error):
                completion(["data":nil])
                
            }
            
        }
    }
    
    func getuser(CREW_ID:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "fetchuser",
            "CREW_ID": CREW_ID,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                
                if let json = response.result.value as? [String:Any]  {
                   // print(json["DATA"]!)
                    
                    if let jsonobject = json["DATA"] as? [Any]  {
                        
                        UserDefaults.standard.set(response.data, forKey: "USERDATA")
                        
                        let notificationName = Notification.Name("userdataset")
                        NotificationCenter.default.post(name: notificationName, object: nil)
                        NotificationCenter.default.removeObserver(self, name: notificationName, object: nil)
                        
                     
                        
                    }
                    
                    
                    completion(["data":nil])
                }
            case .failure(let error):
                completion(["data":nil])
            }
            
        }
    }
    
    func getpicturedata(CREW_ID:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "getpicturedata",
            "CREW_ID": CREW_ID,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        Alamofire.request(apiurl, method: .post, parameters: parameters).response { response in
            
         //   self.reponsedetail(response: response)
            if(response.data != nil)
            {
                if(UIImage(data: response.data!) != nil)
            {
            UserDefaults.standard.set(response.data, forKey: "USERPIC")
            let notificationName = Notification.Name("userpicset")
            NotificationCenter.default.post(name: notificationName, object: nil)
            NotificationCenter.default.removeObserver(self, name: notificationName, object: nil)
            }
                
            }

            
        }
    }
    
    func completedownloadfile(ADD_ID:String!,DOWNLOAD_DATE:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "completedownloadfile",
            "ADD_ID": ADD_ID,
            "DOWNLOAD_DATE":DOWNLOAD_DATE,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                
                if let json = response.result.value as? [Dictionary<String,String>]  {
                    completion(["data":nil])
                }
            case .failure(let error):
                completion(["data":nil])
                
                
            }
            
        }
    }
    
    
    func reportissue(CREW_ID:String!,REPORT_DATE:String!,REPORT:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "reportissue",
            "CREW_ID": CREW_ID,
            "REPORT_DATE":REPORT_DATE,
            "REPORT":REPORT,
            "REPORT_STATUS":"P",
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        
        
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                if let json = response.result.value as? [String:Any]   {
                    if(json["DATA"] != nil)
                    {
                        completion(["data":"1"])
                    }
                    else
                    {
                        completion(["data":nil])
                    }
                    
                }
            case .failure(let error):
                completion(["data":-1])
            }
            
        }
    }
    
    
    func getreportissues(CREW_ID:String!, completion: @escaping ([String:Any?])->())
    {
        let parameters: [String: Any] = [
            "func": "fetchissuereports",
            "CREW_ID": CREW_ID,
            "API_TOKEN": apitoken,
            "DEVICE_ID":deviceid
        ]
        
        Alamofire.request(apiurl, method: .post, parameters: parameters).responseJSON { response in
            
            self.reponsedetail(response: response)
            
            switch response.result {
            case .success:
                
                
                if let json = response.result.value as? [String:Any]  {
                    // print(json["DATA"]!)
                    
                    if let jsonobject = json["DATA"] as? [Any]  {
                        UserDefaults.standard.set(response.data, forKey: "ISSUEREPORTS")
                     completion(["data":jsonobject])
                        
                    }
                    else
                    {
                        completion(["data":nil])
                    }
                    
                    
                    
                }
                else
                {
                   completion(["data":nil])
                }
                
            case .failure(let error):
                completion(["data":-1])
            }
            
        }
    }
    
    
    func reponsedetail(response : DataResponse<Any>)
    {
        
        print("Request: \(String(describing: response.request))")   // original url request
        print("Response: \(String(describing: response.response))") // http url response
        print("Result: \(response.result)")                         // response serialization result
        print("Error: \(response.error)")
        if let json = response.result.value {
            print("JSON: \(json)") // serialized json response
        }
        
        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            print("Data: \(utf8Text)") // original server data as UTF8 string
        }
        
    }
    
}
