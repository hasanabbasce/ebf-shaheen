//
//  DownloadService.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 23/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Alamofire

// Downloads song snippets, and stores in local file.
// Allows cancel, pause, resume download.
class DownloadService {
    
    var fileid: String?
    var title: String?
    var filename: String?
    var request: Alamofire.Request?
    var downloaded:Bool = false
    var filepath: String?
    var progress : Double = 0
    var category : String?
    var ispause : Bool = false
    var idlestate : Bool = false
    var filesize : String?
    var dowloadeddate : String?
    var newversion : Bool = false
    var uploaddate : String?
    var refdoc : String?
    var version : String?
    var adm_id : String?
    
    init(fileid:String , title: String,filename: String,category:String){
        self.fileid = fileid
        self.title = title
        self.filename = filename
        self.category = category
        print("ss")
    }
    
    func startDownload(completion: @escaping ([String:Any?])->()) {


        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as    NSURL
            
            print("***documentURL: ",documentsURL)
            
            let PDF_name : String = self.filename!
            
            let fileURL = documentsURL.appendingPathComponent(PDF_name)
            
            print("***fileURL: ",fileURL ?? "")
            
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
            
        }
        
      
        let escapedfilename = self.filename!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let fileurltodownload = URL(string: "http://172.16.24.205/efb/?category=" + self.category! + "&filename=" + escapedfilename!)!
        self.request = BackendAPIManager.sharedInstance.alamoFireManager.download(fileurltodownload, to: destination).downloadProgress(closure: { (prog) in
            self.progress = prog.fractionCompleted
            completion(["Progress":prog.fractionCompleted])
            print("Progress: \(prog.fractionCompleted)")
            
        }).response { response in
            
            
            if response.error == nil, let filepathh = response.destinationURL?.path    {
                
                print("Downloaded")                //                print(filepathh)
                self.filepath = filepathh
                self.downloaded = true
                completion(["Complete":filepathh])
                
            }
            else
            {
                print(response.error?.localizedDescription)
                completion(["ERROR":response.error?.localizedDescription])
            }
            
        }
        
    }
    
    func pauseDownload() {
        self.request?.suspend()
        self.ispause = true
    }
    
    func resumeDownload() {
        self.request?.resume()
        self.ispause = false
    }
    
    func cancelDownload() {
        self.request?.cancel()
        self.ispause = false
        self.progress = 0
    }
}
